package ru.amv.us.model;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

public class FileInfoGrabber {
	private String path;

	public FileInfoGrabber(String path) {
		this.path = path;
	}

	public List<FileInfo> getFileList() throws NullPointerException, IOException, NoSuchAlgorithmException {
		return getFileList(path);
	}

	public List<FileInfo> getFileList(String url) throws NullPointerException, IOException, NoSuchAlgorithmException {
		List<FileInfo> fileInfos = new ArrayList<>();

		File[] files = new File(url).listFiles(File::isFile); // Only files, because on folder getMD5 throws exception

		for (File file : files) {
			//System.out.println(file.getName());
			FileInfo fileInfo = new FileInfo();
			fileInfo.setName(file.getName());
			fileInfo.setSize(file.length());
			fileInfo.setHash(getMD5(file.toPath()));
			fileInfos.add(fileInfo);
		}
		return fileInfos;
	}

	public String getJsonFilesInfo() throws IOException, NoSuchAlgorithmException {
		List<FileInfo> fileInfos = getFileList(path);
		ObjectMapper objectMapper = new ObjectMapper();
		return objectMapper.writeValueAsString(fileInfos);
	}

	private String getMD5(Path path) throws NoSuchAlgorithmException, IOException {
		byte[] hash = MessageDigest.getInstance("MD5").digest(Files.readAllBytes(path));
		return new BigInteger(1, hash).toString(16);
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}
}
