package ru.amv.us.model;

public class FileInfo {
	private String name;
	private String hash;
	private long size;

	public FileInfo(String name, String hash, long size) {
		this.name = name;
		this.hash = hash;
		this.size = size;
	}

	public FileInfo() {
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getHash() {
		return hash;
	}

	public void setHash(String hash) {
		this.hash = hash;
	}

	public long getSize() {
		return size;
	}

	public void setSize(long size) {
		this.size = size;
	}
}
