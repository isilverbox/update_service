package ru.amv.us.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.amv.us.model.FileInfoGrabber;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;

@Controller
//@ComponentScan
@RequestMapping("/smapp")
//@RequestMapping("/")

public class LibController {

//	@GetMapping("/lib")
//	public String lib(@RequestParam(name="name", required=false, defaultValue="World") String name, Model model){
//		model.addAttribute("name", name);
//		return "greeting";
//	}

	@GetMapping("/app/lib")
	public String getLibInfo(Model model) {
		FileInfoGrabber fileInfoGrabber = new FileInfoGrabber("C:\\Smapp\\lib");
		try {
			String json = fileInfoGrabber.getJsonFilesInfo();//("C:\\Smapp\\lib");//todo change path
			model.addAttribute("fileInfo", json);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return "fileInfo";
	}

	@GetMapping("/app")
	public String getAppInfo(Model model) {
		FileInfoGrabber fileInfoGrabber = new FileInfoGrabber("C:\\Smapp");
		try {
			String json = fileInfoGrabber.getJsonFilesInfo();//("C:\\Smapp\\lib");//todo change path
			model.addAttribute("fileInfo", json);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return "fileInfo";
	}

	@GetMapping("")

	public String getSmappPage(Model model) {

		return "smapp";
	}
}
