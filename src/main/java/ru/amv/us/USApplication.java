package ru.amv.us;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class USApplication {

	public static void main(String[] args) {
		SpringApplication.run(USApplication.class, args);

//		SpringApplication app = new SpringApplication(SpringMain.class);
//		app.setDefaultProperties(Collections.<String, Object>singletonMap("server.port", "8083"));
//		app.run(args);
	}

//	@GetMapping("/hello")
//	public String hello(@RequestParam(value = "name", defaultValue = "World") String name) {
//		return String.format("Hello %s!", name);
//	}
}
